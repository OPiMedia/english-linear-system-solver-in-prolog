:- module(linprog, [analyze/2,
                    lpsolve/2,
                    lpsolve_analyze/1,
                    run_example/0]).
/** <module> linprog

"Declarative Programming" Assessed Assignment 2:
A System for Linear Programming in English.

This the implementation of a solver of linear equations set
written in mathematical notation or English language.

Open HTML dynamic documentation in a browser
(then click on the link linprog.pl and after that on the arrow "Click to include private"):
$ prolog --pldoc linprog.pl

Some intermediary predicates (and some pieces of the grammar) are not in this HTML documentation.

To solve the example problem from the section 7 of the statement and write results:
?- run_example.

To run all unit tests (see below, after the real code, and a copy of the output):
?- run_tests.

To run only the test on the set of sentences example from the section 7 of the project statement
?- run_tests(test_example_section_7).

Tested with SWI-Prolog version 7.2.3 for amd64
on Debian GNU/Linux 9 (stretch) 64 bits.

@author Olivier Pirson --- http://www.opimedia.be/
@copyright GPLv3 --- 2018 Olivier Pirson
@version June 17, 2018
*/

:- use_module(library(assoc)).
:- use_module(library(clpfd)).

% :- assertz(clpfd:full_answer).



% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Helper predicates for arithmetic operations %
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% addition(A, B, R)
% Succeeds iff R is A + B.
%
% @param A integer finite domain
% @param B integer finite domain
% @param C integer finite domain
addition(A, B, R) :- R #= A + B.

%% substraction(A, B, R)
% Succeeds iff R is A - B.
%
% @param A integer finite domain
% @param B integer finite domain
% @param C integer finite domain
substraction(A, B, R) :- R #= A - B.

%% multiplication(A, B, R)
% Succeeds iff R is A * B.
%
% @param A integer finite domain
% @param B integer finite domain
% @param C integer finite domain
multiplication(A, B, R) :- R #= A * B.

%% division(A, B, R)
% Succeeds iff R is A / B (the integer division).
% If B = 0 then throws an exception zero_divisor.
%
% @param A integer finite domain
% @param B integer finite domain
% @param C integer finite domain
division(_A, 0, _R) :- throw(error(evaluation_error(zero_divisor), division)).
division(A, B, R) :- B \= 0,
                     R #= A div B.  % exception zero_divisor if B is 0

%% remainder(A, B, R)
% Succeeds iff R is A % B (the remainder of the integer division A / B).
% If B = 0 then throws an exception zero_divisor.
%
% @param A integer finite domain
% @param B integer finite domain
% @param C integer finite domain
remainder(_A, 0, _R) :- throw(error(evaluation_error(zero_divisor), remainder)).
remainder(A, B, R) :- B \= 0,
                      R #= A rem B.  % exception zero_divisor if B is 0



% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Helper predicates for inequalities and ranges %
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% less(A, B)
% Succeeds iff A < B
%
% @param A integer finite domain
% @param B integer finite domain
less(A, B) :- A #< B.

%% greater(A, B)
% Succeeds iff A > B
%
% @param A integer finite domain
% @param B integer finite domain
greater(A, B) :- A #> B.

%% less_or_equal(A, B)
% Succeeds iff A <= B
%
% @param A integer finite domain
% @param B integer finite domain
less_or_equal(A, B) :- A #=< B.

%% greater_or_equal(A, B)
% Succeeds iff A >= B
%
% @param A integer finite domain
% @param B integer finite domain
greater_or_equal(A, B) :- A #>= B.


%% greater_or_equal(A, B)
% Succeeds iff R is a range that contains values between A to B (included).
% (If A > B, then the range is like B to A.)
%
% @param A integer finite domain
% @param B integer finite domain
% @param R integer finite domain
range(A, B, R) :- A #=< B,
                  greater_or_equal(R, A),
                  less_or_equal(R, B).
range(A, B, R) :- A #> B,
                  greater_or_equal(R, B),
                  less_or_equal(R, A).


% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Helper predicates to deal with lists of pair variable name: Prolog variable %
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% listvariables_contains(ListVariables, Variable, Result)
% If Variable is a variable name contained in the list
% then succeeds iff Result is the corresponding Variable: Prolog variable,
% else succeeds iff Result is false.
%
% @param ListVariables list of pairs variable name: Prolog variable
% @param Variable variable name
% @param Result false or a pair variable name: Prolog variable
listvariables_contains([], _Variable, false).
listvariables_contains([Variable:PrologVariable|_RemainListVariables], Variable, Variable:PrologVariable).
listvariables_contains([Var:_PrologVar|RemainListVariables], Variable, Result) :-
    Var \= Variable,
    listvariables_contains(RemainListVariables, Variable, Result).

%% listvariables_add(ListVariables, Variable:PrologVariable, NewListVariables)
% Succeeds iff NewListVariables is [Variable:PrologVariable|ListVariables].
%
% @param ListVariables list of pairs variable name: Prolog variable
% @param Variable:PrologVariable pairs variable name: Prolog variable
% @param NewListVariables list of pairs variable name: Prolog variable
listvariables_add(ListVariables, Variable:PrologVariable, [Variable:PrologVariable|ListVariables]).

%% listvariables_get(ListVariables, Variable, PrologVariable)
% If Variable:PrologVariable is contained in the list
% then succeeds
% else succeeds iff Variable and PrologVariable are false.
%
% @param ListVariables list of pairs variable name: Prolog variable
% @param Variable variable name
% @param PrologVariable Prolog variable
listvariables_get([], false, false).
listvariables_get([Variable:PrologVariable | _RemainListVariables], Variable, PrologVariable).
listvariables_get([OtherVariable:_OtherPrologVariable | RemainListVariables], Variable, PrologVariable) :-
    OtherVariable \= Variable,
    listvariables_get(RemainListVariables, Variable, PrologVariable).

%% listvariables_write(ListVariables)
% Writes each variable name with its domain (or its value if it is unique).
% (Prints the list in the reverse order,
% that correspond to the order of adding.)
%
% @param ListVariables list of pairs variable name: Prolog variable
listvariables_write(ListVariables) :-
    reverse(ListVariables, ReversedListVariables),
    listvariables_write_loop(ReversedListVariables).

listvariables_write_loop([]).
listvariables_write_loop([Variable:PrologVariable | RemainListVariables]) :-
    fd_size(PrologVariable, Size),
    Size = 1,
    format("~w = ~w~n", [Variable, PrologVariable]),
    listvariables_write_loop(RemainListVariables).
listvariables_write_loop([Variable:PrologVariable | RemainListVariables]) :-
    fd_size(PrologVariable, Size),
    Size \= 1,
    fd_inf(PrologVariable, Inf),
    fd_sup(PrologVariable, Sup),
    format("~w in ~w..~w~n", [Variable, Inf, Sup]),
    listvariables_write_loop(RemainListVariables).



% %%%%%%%%%%%%%%%
% Grammar (DCG) %
% %%%%%%%%%%%%%%%

% Operator symbols and equivalent words

negative_symbol --> [-].

addition_symbol --> [+].
addition_symbol --> [plus].

substraction_symbol --> [-].
substraction_symbol --> [minus].

multiplication_symbol --> [*].
multiplication_symbol --> [times].

division_symbol --> [/].
division_symbol --> [dividing].
division_symbol --> [quotient, of].

remainder_symbol --> [\45].  % symbol % for remainder of the integer division

equality_symbol --> [=].
equality_symbol --> [equals].
equality_symbol --> [is].
equality_symbol --> [contains].
equality_symbol --> [holds, the, result, of].

less_symbol --> [<].
less_symbol --> [is, less, than].

greater_symbol --> [>].
greater_symbol --> [is, greater, than].

less_or_equal_symbol --> [<=].
less_or_equal_symbol --> [is, less, than, or, equal, to].

greater_or_equal_symbol --> [>=].
greater_or_equal_symbol --> [is, greater, than, or, equal, to].


% Some groups of equivalent words

and_or_by --> [and].
and_or_by --> [by].

dividing_or_division_or_quotient --> [result, of, dividing].
dividing_or_division_or_quotient --> [division].
dividing_or_division_or_quotient --> [quotient].

lies_or_is --> [lies].
lies_or_is --> [is].



%% number(Number, Li, Lo)
% Succeeds iff the first term of Li is the representation of an integer number,
% Number is the domain corresponding to this unique value,
% and the rest of Li is Lo.
%
% @param Number integer finite domain with a unique value
% @param Li list of terms
% @param Lo list of terms
number(Number) --> [N],
                   {integer(N),
                    Number in N..N}.


%% is_variable(Variable, Li, Lo)
% Succeeds iff the first term of Li is the representation of a variable name,
% and the rest of Li is Lo.
%
% @param Variable variable name
% @param Li list of terms
% @param Lo list of terms
is_variable(Variable) --> [Variable],
                          {nonvar(Variable),
                           name(Variable, [Char|[]]),  % only one character
                           97 =< Char, Char =< 122}.   % lower case

%% variable(VariablesIn, VariablesOut, Variable, PrologVariable, Li, Lo)
% Succeeds iff the first term of Li is Variable the representation of a variable name,
% PrologVariable is the associated Prolog variable
% and the rest of Li is Lo.
% If VariablesIn contains Variable
% then succeeds iff VariablesOut is VariablesIn,
% else succeeds iff VariablesOut is [Variable:PrologVariable | VariablesIn].
%
% @param VariablesIn list of pairs variable name: Prolog variable
% @param VariablesOut list of pairs variable name: Prolog variable
% @param Variable variable name
% @param PrologVariable Prolog variable
% @param Li list of terms
% @param Lo list of terms
variable(VariablesIn, VariablesOut, Variable, PrologVariable) -->  % new variable, adds to the list
    is_variable(Variable),
    {listvariables_contains(VariablesIn, Variable, false),
     listvariables_add(VariablesIn, Variable:PrologVariable, VariablesOut),
     PrologVariable in inf..sup}.
variable(VariablesIn, VariablesIn, Variable, PrologVariable) -->  % variable, already in the list
    is_variable(Variable),
    {listvariables_contains(VariablesIn, Variable, Variable:PrologVariable),
     PrologVariable in inf..sup}.



% Arithmetic expressions, in several steps to respect priority order:
%   1) * / %
%   2) + -

atomic_exp(VariablesIn, VariablesIn, R) --> number(R).
atomic_exp(VariablesIn, VariablesOut, R) --> variable(VariablesIn, VariablesOut, _Variable, R).

unary_exp(VariablesIn, VariablesOut, R) -->
    atomic_exp(VariablesIn, VariablesOut, R).
unary_exp(VariablesIn, VariablesOut, R) -->
    negative_symbol, unary_exp(VariablesIn, VariablesOut, R).

multiplicative_exp(VariablesIn, VariablesOut, R) -->
    unary_exp(VariablesIn, VariablesOut, R).
multiplicative_exp(VariablesIn, VariablesOut, R) -->  % *
    unary_exp(VariablesIn, VariablesMiddle, A),
        multiplication_symbol, multiplicative_exp(VariablesMiddle, VariablesOut, B),
    {multiplication(A, B, R)}.
multiplicative_exp(VariablesIn, VariablesOut, R) -->  % / (integer division)
    unary_exp(VariablesIn, VariablesMiddle, A),
        division_symbol, multiplicative_exp(VariablesMiddle, VariablesOut, B),
    {division(A, B, R)}.
multiplicative_exp(VariablesIn, VariablesOut, R) -->  % % (remainder of the integer division)
    unary_exp(VariablesIn, VariablesMiddle, A),
        remainder_symbol, multiplicative_exp(VariablesMiddle, VariablesOut, B),
    {remainder(A, B, R)}.

additive_exp(VariablesIn, VariablesOut, R) -->
    multiplicative_exp(VariablesIn, VariablesOut, R).
additive_exp(VariablesIn, VariablesOut, R) -->  % +
    multiplicative_exp(VariablesIn, VariablesMiddle, A),
        addition_symbol, additive_exp(VariablesMiddle, VariablesOut, B),
    {addition(A, B, R)}.
additive_exp(VariablesIn, VariablesOut, R) -->  % -
    multiplicative_exp(VariablesIn, VariablesMiddle, A),
        substraction_symbol, additive_exp(VariablesMiddle, VariablesOut, B),
    {substraction(A, B, R)}.

arithmetic_exp(VariablesIn, VariablesOut, R) -->
    additive_exp(VariablesIn, VariablesOut, R).



% Arithmetic expressions expressed in mathematical notation or English language
expression(VariablesIn, VariablesOut, R) -->
    arithmetic_exp(VariablesIn, VariablesOut, R).
expression(VariablesIn, VariablesOut, R) --> % +
    [the, sum, of], arithmetic_exp(VariablesIn, VariablesMiddle, A),
        [and], arithmetic_exp(VariablesMiddle, VariablesOut, B),
    {addition(A, B, R)}.
expression(VariablesIn, VariablesOut, R) --> % -
    [the, difference, of], arithmetic_exp(VariablesIn, VariablesMiddle, A),
        [and], arithmetic_exp(VariablesMiddle, VariablesOut, B),
    {substraction(A, B, R)}.
expression(VariablesIn, VariablesOut, R) --> % *
    [the, multiplication, of], arithmetic_exp(VariablesIn, VariablesMiddle, A),
        [and], arithmetic_exp(VariablesMiddle, VariablesOut, B),
    {multiplication(A, B, R)}.
expression(VariablesIn, VariablesOut, R) --> % *
    [the, product, of], arithmetic_exp(VariablesIn, VariablesMiddle, A),
        [and], arithmetic_exp(VariablesMiddle, VariablesOut, B),
    {multiplication(A, B, R)}.
expression(VariablesIn, VariablesOut, R) --> % / (integer division)
    [the], dividing_or_division_or_quotient, [of], arithmetic_exp(VariablesIn, VariablesMiddle, A),
        and_or_by, arithmetic_exp(VariablesMiddle, VariablesOut, B),
    {division(A, B, R)}.
expression(VariablesIn, VariablesOut, R) --> % / (integer division)
    [dividing], arithmetic_exp(VariablesIn, VariablesMiddle, A),
        [by], arithmetic_exp(VariablesMiddle, VariablesOut, B),
    {division(A, B, R)}.
expression(VariablesIn, VariablesOut, R) --> % % (remainder of the integer division)
    [the, remainder, of, the, division, of], arithmetic_exp(VariablesIn, VariablesMiddle, A),
        and_or_by, arithmetic_exp(VariablesMiddle, VariablesOut, B),
    {remainder(A, B, R)}.



% Equalities, inequalities and range sentences with expression
sentence_equal(VariablesIn, VariablesOut, _LastVariableIn, LastVariableOut) -->  % =
    variable(VariablesIn, VariablesMiddle, LastVariableOut, PrologVariable),
        equality_symbol, expression(VariablesMiddle, VariablesOut, Expression),
        {PrologVariable #= Expression}.

sentence_equal(VariablesIn, VariablesOut, _LastVariableIn, LastVariableOut) -->  % is greater than or equal to
    variable(VariablesIn, VariablesMiddle, LastVariableOut, PrologVariable),
        greater_or_equal_symbol, expression(VariablesMiddle, VariablesOut, Expression),
        {greater_or_equal(PrologVariable, Expression)}.
sentence_equal(VariablesIn, VariablesOut, _LastVariableIn, LastVariableOut) -->  % is less than or equal to
    variable(VariablesIn, VariablesMiddle, LastVariableOut, PrologVariable),
        less_or_equal_symbol, expression(VariablesMiddle, VariablesOut, Expression),
        {less_or_equal(PrologVariable, Expression)}.

sentence_equal(VariablesIn, VariablesOut, _LastVariableIn, LastVariableOut) -->  % is greater than
    variable(VariablesIn, VariablesMiddle, LastVariableOut, PrologVariable),
        greater_symbol, expression(VariablesMiddle, VariablesOut, Expression),
        {greater(PrologVariable, Expression)}.
sentence_equal(VariablesIn, VariablesOut, _LastVariableIn, LastVariableOut) -->  % is less than
    variable(VariablesIn, VariablesMiddle, LastVariableOut, PrologVariable),
        less_symbol, expression(VariablesMiddle, VariablesOut, Expression),
        {less(PrologVariable, Expression)}.

sentence_equal(VariablesIn, VariablesOut, _LastVariableIn, LastVariableOut) -->  % lies between
    variable(VariablesIn, VariablesMiddle1, LastVariableOut, PrologVariable),
        lies_or_is, [between], expression(VariablesMiddle1, VariablesMiddle2, ExpressionA),
        [and], expression(VariablesMiddle2, VariablesOut, ExpressionB),
        {range(ExpressionA, ExpressionB, PrologVariable)}.
sentence_equal(VariablesIn, VariablesOut, _LastVariableIn, LastVariableOut) -->  % varies from
    variable(VariablesIn, VariablesMiddle1, LastVariableOut, PrologVariable),
        [varies, from], expression(VariablesMiddle1, VariablesMiddle2, ExpressionA),
        [to], expression(VariablesMiddle2, VariablesOut, ExpressionB),
        {range(ExpressionA, ExpressionB, PrologVariable)}.
sentence_equal(VariablesIn, VariablesOut, _LastVariableIn, LastVariableOut) -->  % is in the range
    variable(VariablesIn, VariablesMiddle1, LastVariableOut, PrologVariable),
        [is, in, the, range], expression(VariablesMiddle1, VariablesMiddle2, ExpressionA),
        [to], expression(VariablesMiddle2, VariablesOut, ExpressionB),
        {range(ExpressionA, ExpressionB, PrologVariable)}.
sentence_equal(VariablesIn, VariablesOut, _LastVariableIn, LastVariableOut) -->  % is in ..
    variable(VariablesIn, VariablesMiddle1, LastVariableOut, PrologVariable),
        [is, in], expression(VariablesMiddle1, VariablesMiddle2, ExpressionA),
        [..], expression(VariablesMiddle2, VariablesOut, ExpressionB),
        {range(ExpressionA, ExpressionB, PrologVariable)}.



% Sentence eventually begining by "Variable", "A variable" or "The variable" with sentence_equal
sentence_variable(VariablesIn, VariablesOut, LastVariableIn, LastVariableOut) -->
    sentence_equal(VariablesIn, VariablesOut, LastVariableIn, LastVariableOut).

sentence_variable(VariablesIn, VariablesOut, LastVariableIn, LastVariableOut) -->  % Variable ...
    [variable], sentence_equal(VariablesIn, VariablesOut, LastVariableIn, LastVariableOut).

sentence_variable(VariablesIn, VariablesOut, LastVariableIn, LastVariableOut) -->  % Valid A variable ...
    [a, variable], sentence_equal(VariablesIn, VariablesOut, LastVariableIn, LastVariableOut),
    {\+ listvariables_contains(VariablesIn, LastVariableOut, LastVariableOut:_PrologVariable)}.
sentence_variable(VariablesIn, _VariablesOut, _LastVariableIn, _LastVariableOut) --> % Invalid A variable ... (because variable already exist)
    [a, variable], variable(VariablesIn, _VariablesMiddle, Variable, PrologVariable),
    {listvariables_contains(VariablesIn, Variable, Variable:PrologVariable),
     throw(error(evaluation_error(a_variable_already_exist), sentence_variable))}.

sentence_variable(VariablesIn, VariablesOut, LastVariableIn, LastVariableOut) -->  % The variable ...
    [the, variable], sentence_equal(VariablesIn, VariablesOut, LastVariableIn, LastVariableOut).



% Sentence eventually begining by "It" or "All  these variables are" with sentence_variable
% Part of the grammar dealt with explicit predicates ( :- instead --> )
% to have the possibility to take the replace the beginning of the sentence
% with the variable name corresponding to "It"
% or with a collection of sentences for each existing variable names.
sentence_it_all(VariablesIn, VariablesOut, LastVariableIn, LastVariableOut) -->
    sentence_variable(VariablesIn, VariablesOut, LastVariableIn, LastVariableOut).

sentence_it_all(VariablesIn, VariablesOut, LastVariableIn, LastVariableIn,
                [it|Li], []) :-  % Valid It ...
    listvariables_contains(VariablesIn, LastVariableIn, LastVariableIn:_PrologVariable),
        sentence_variable(VariablesIn, VariablesOut, LastVariableIn, LastVariableIn,
                          [LastVariableIn|Li], []).
sentence_it_all(VariablesIn, _VariablesOut, LastVariableIn, _LastVariableOut,
                [it|_Li], []) :-  % Invalid It ...
    % (because first sentence, previous is All ..., or LastVariableIn)
    \+ listvariables_contains(VariablesIn, LastVariableIn, LastVariableIn:_PrologVariable),
    throw(error(evaluation_error(it_with_unknown_variable), sentence_it_all)).

sentence_it_all(VariablesIn, VariablesOut, _LastVariableIn, all,
                [all, these, variables, are|Li], []) :-  % All these variables are ...
    sentence_all_loop(VariablesIn, VariablesOut, VariablesIn, Li).

sentence_all_loop(_VariablesIn, _VariablesOut, [], _Li).
sentence_all_loop(VariablesIn, VariablesOut, [Variable:PrologVariable|RemainVariables], Li) :-
    listvariables_contains(VariablesIn, Variable, Variable:PrologVariable),
        sentence_variable(VariablesIn, VariablesOut, all, Variable, [Variable, is|Li], []),
        sentence_all_loop(VariablesIn, VariablesOut, RemainVariables, Li).
sentence_all_loop(VariablesIn, _VariablesOut, [Variable:PrologVariable|_RemainVariables], _Li) :-
    \+ listvariables_contains(VariablesIn, Variable, Variable:PrologVariable),
    throw(error(evaluation_error(all_with_unknown_variable), sentence_all_loop)).



% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Main predicate to solve one sentence %
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% sentence(Sentence, VariablesIn, VariablesOut, LastVariableIn, LastVariableOut, Li, Lo)
% The most general sentence accepted (in list of terms form or string form).
% (You can see the multiple examples on unit tests below.)
% Succeeds iff Sentence is a well formed sentence,
% VariablesOut is VariablesIn with new variable(s)
% and the rest of Li is Lo.
%
% LastVariableIn is the last variable name used in a previous sentence,
% or false if this is the first,
% or all if the previous is a "All these variables..." sentence.
%
% LastVariableOut is the main variable name used in this sentence,
% or all if it is a "All these variables..." sentence.
%
% @param Sentence list of terms or string
% @param VariablesIn list of pairs variable name: Prolog variable
% @param VariablesOut list of pairs variable name: Prolog variable
% @param LastVariableIn false, all or variable name
% @param LastVariableOut false, all or variable name
% @param Li list of terms
% @param Lo list of terms
sentence(StringSentence, VariablesIn, VariablesOut, LastVariableIn, LastVariableOut) :-
    string(StringSentence),
    string_lower(StringSentence, LowerStringSentence),
    split(LowerStringSentence, StringsSentence),
    conv(StringsSentence, Sentence),
    sentence_it_all(VariablesIn, VariablesOut, LastVariableIn, LastVariableOut, Sentence, []).
sentence(Sentence, VariablesIn, VariablesOut, LastVariableIn, LastVariableOut) :-
    \+ string(Sentence),
    sentence_it_all(VariablesIn, VariablesOut, LastVariableIn, LastVariableOut, Sentence, []).



% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Helper predicates to convert string sentence in terms sentence %
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% split(String, Sentence)
% Succeeds iff Sentence is a list of strings
% corresponding to the split of String around the spaces.
%
% @param String
% @param Sentence
split(String, Sentence) :-
    split_string(String, " ", " ", Sentence).

%% conv(StringsSentence, Sentence)
% Succeeds iff Sentence is the conversion of StringsSentence in list of terms.
%
% @param StringsSentence
% @param Sentence
conv([], []).
conv(["%" | RemainStringsSentence], [\45|RemainSentence]) :-
    conv(RemainStringsSentence, RemainSentence).
conv([String | RemainStringsSentence], [Term|RemainSentence]) :-
    assertion(string(String)),
    String \= "%",
    term_string(Term, String),
    conv(RemainStringsSentence, RemainSentence).



% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Main predicates to solve and analyze a problem expressed by a collection of sentences %
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% sentences(Sentences, VariablesIn, VariablesOut, LastVariableIn, LastVariableOut)
% Succeeds iff each sentence of Sentences succeeds
% and all constraint give possible solution.
%
% This solves the problem expressed by the collection of sentences
% with initial list of variables in VariablesIn
% and the initial variable LastVariableIn
% like in sentence/7.
%
% VariablesOut is VariablesIn with new variable(s).
%
% LastVariableOut is the main variable used in last sentence.
%
% @param Sentences list of sentences (in terms form or string form)
% @param VariablesIn list of pairs variable name: Prolog variable
% @param VariablesOut list of pairs variable name: Prolog variable
% @param LastVariableIn false, all or variable name
% @param LastVariableOut false, all or variable name
sentences([], VariablesIn, VariablesIn, LastVariableIn, LastVariableIn).
sentences([Sentence|RemainSentences], VariablesIn, VariablesOut, LastVariableIn, LastVariableOut) :-
    sentence(Sentence, VariablesIn, VariablesMiddle, LastVariableIn, LastVariableMiddle),
    sentences(RemainSentences, VariablesMiddle, VariablesOut, LastVariableMiddle, LastVariableOut).


%% lpsolve(Sentences, VariablesOut)
% Succeeds iff each sentence of Sentences succeeds
% and all constraint give possible solution.
%
% This solves the problem expressed by the collection of sentences
% and VariablesOut contains the list of pairs variable name: Prolog variable.
%
% @param Sentences list of sentences (in terms form or string form)
% @param VariablesOut list of pairs variable name: Prolog variable
lpsolve(Sentences, VariablesOut) :-
    sentences(Sentences, [], VariablesOut, false, _LastVariableOut).


%% lpsolve_analyze(Sentences)
% Succeeds iff lpsolve(Sentences, VariablesOut)
% and write the analysis of VariablesOut.
lpsolve_analyze(Sentences) :-
    call_residue(lpsolve(Sentences, VariablesOut), Residues),
    analyze(VariablesOut, Residues).



%% analyze(ListVariables, Residues)
% Writes if the solution is unique, finite or maybe infinite.
% Then writes by categorie each variable name with its corresponding unique value or domain.
% When there is more than one solution,
% writes also surviving constraints.
%
% Surviving constraints limitation:
%
% The (partial) implementation of call_residue/2 (see below)
% gives Prolog variables that don't correspond
% to the Prolog variables in the ListVariables.
% So it seem be "impossible" to replace it by variable names like asked.
%
% A workaround is implemented.
% It identifies some variables with a comparison of their domains.
% If a same range is found for a Prolog variable
% this implementation consider that is the same variable.
% It is much more a partial hack than an elegant complete implementation.
%
% @param ListVariables list of pairs variable name: Prolog variable
% @param Residues list in the format output by call_residue/2
analyze(ListVariables, _Residues) :-
    split_variables(ListVariables, Uniques, [], []),
    format("---------- Unique solution ----------~n"),
    listvariables_write(Uniques),
    format("----------~n").
analyze(ListVariables, Residues) :-
    split_variables(ListVariables, Uniques, NotUniques, []),
    NotUniques \= [],
    format("---------- More than one solution, but finite number of solutions ----------~n"),
    format("----- Variable(s) with unique value -----~n"),
    listvariables_write(Uniques),
    format("----- Variable(s) with more than one values but finite -----~n"),
    listvariables_write(NotUniques),
    format("----- Constraint residues -----~n"),
    residues_write(ListVariables, Residues),
    format("----------~n").
analyze(ListVariables, _Residues) :-
    split_variables(ListVariables, Uniques, NotUniques, Finites),
    NotUniques \= [],
    Finites \= [],
    format("---------- Maybe an infinite number of solutions ----------~n"),
    format("----- Variable(s) with unique value -----~n"),
    listvariables_write(Uniques),
    format("----- Variable(s) with more than one values but finite -----~n"),
    listvariables_write(NotUniques),
    format("----- Variable(s) with infinite numbers of values -----~n"),
    listvariables_write(Finites),
    format("----------~n").



% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Helper predicates to analyze results %
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% range_equals(A, B)
% Succeeds iff domains A and B are equal.
%
% @param A a finite integer domain that is a range
% @param B a finite integer domain that is a range
range_equals(A, B) :-
    fd_inf(A, InfA),
    fd_inf(B, InfB),
    InfA = InfB,
    fd_sup(A, SupA),
    fd_sup(B, SupB),
    SupA = SupB.


%% replace_substring(String, Substring, NewSubstring, NewString)
% Succeeds iff NewString is String
% such that all Substring contained in String is replaced by NewSubstring.
%
% @param String string
% @param Substring not empty string
% @param NewSubstring string
% @param NewString string
replace_substring(String, Substring, NewSubstring, NewString) :-
    assertion(Substring \= ""),
    string_to_list(String, List),
    string_to_list(Substring, Sublist),
    string_to_list(NewSubstring, NewSublist),
    replace_sublist(List, Sublist, NewSublist, NewList),
    string_to_list(NewString, NewList).


%% replace_sublist(List, Sublist, NewSublist, NewList)
% Succeeds iff NewList is List
% such that all Sublist contained in List is replaced by NewSublist.
%
% @param List list
% @param Sublist not empty list
% @param NewSublist list
% @param NewList list
replace_sublist([], _Sublist, _NewSublist, []).
replace_sublist([Item|RemainList], Sublist, NewSublist, [Item|RemainNewList]) :-
    assertion(Sublist \= []),
    \+ prefix(Sublist, [Item|RemainList]),
    replace_sublist(RemainList, Sublist, NewSublist, RemainNewList).
replace_sublist(List, Sublist, NewSublist, NewList) :-
    assertion(Sublist \= []),
    append(Sublist, RemainList, List),
    replace_sublist(RemainList, Sublist, NewSublist, RemainNewList),
    append(NewSublist, RemainNewList, NewList).


%% write_list(List)
% Write each item of the list on a separate line.
%
% @param List a list
write_list([]).
write_list([Item|RemainList]) :-
    format("~w~n", [Item]),
    write_list(RemainList).


%% residues_write(ListVariables, Residues)
% Write each residues with a partial identification of variable names
% like explained in analyze/2.
%
% @param ListVariables list of pairs variable name: Prolog variable
% @param Residues list in the format output by call_residue/2
residues_write(ListVariables, [_-Residues]) :-
    residues_identify_variable(Residues, ListVariables, ListResidueVariables),
    residues_clean(Residues, ListResidueVariables, CleanedResidues),
    write_list(CleanedResidues).


%% residues_identify_variable(Residues, ListVariables, ListResidueVariables)
% Succeeds iff ListResidueVariables is the of pairs variable name: residue Prolog variable
% successfully identified with ListVariables the list of pairs variable name: Prolog variable
% in Residues the list of residues.
%
% @param Residues list of residues (the good part from the format output by call_residue/2)
% @param ListVariables list of pairs variable name: Prolog variable
% @param ListResidueVariables list of pairs variable name: residue Prolog variable
residues_identify_variable([], _ListVariables, []).
residues_identify_variable([Residue|RemainResidues], ListVariables, ListResidueVariables) :-
    (_: (ResiduePrologVariable in X..Y)) = Residue,
    Range in X..Y,
    sublist_same_range(ListVariables, Range, PairVariables),
    % If one and only one found, then it is correctly identified, else never mind
    ([Variable:_PrologVariable] = PairVariables ->
         ListResidueVariables = [Variable:ResiduePrologVariable | RemainListResidueVariables];
         ListResidueVariables = RemainListResidueVariables),
    residues_identify_variable(RemainResidues, ListVariables, RemainListResidueVariables).
residues_identify_variable([Residue|RemainResidues], ListVariables, ListResidueVariables) :-
    (_: (_ResiduePrologVariable in _X.._Y)) \= Residue,
    residues_identify_variable(RemainResidues, ListVariables, ListResidueVariables).


%% sublist_same_range(ListVariables, Range, IdentifiedVariables)
% Succeeds iff IdentifiedVariables is the sublist of variable name: Prolog variable
% from ListVariables that is a range equals to Range.
%
% @param ListVariables list of pairs variable name: Prolog variable
% @param Range a finite integer domain that is a range
% @param IdentifiedVariables list of pairs variable name: Prolog variable
sublist_same_range([], _Range, []).
sublist_same_range([Variable:PrologVariable | RemainListVariables], Range, [Variable:PrologVariable|IdentifiedVariables]) :-
    range_equals(PrologVariable, Range),  % guess PrologVariable is also a range
    sublist_same_range(RemainListVariables, Range, IdentifiedVariables).
sublist_same_range([_Variable:PrologVariable | RemainListVariables], Range, IdentifiedVariables) :-
    \+ range_equals(PrologVariable, Range),
    sublist_same_range(RemainListVariables, Range, IdentifiedVariables).


%% residues_clean(Residues, ListResidueVariables, StringResidues)
% Succeeds iff StringResidues is a list of clean string version of each residue of Residues%
% The Prolog variables identified in ListResidueVariables
% are replaced by the corresponding variable name.
%
% @param Residues list of residues (the good part from the format output by call_residue/2)
% @param ListResidueVariables list of pairs variable name: residue Prolog variable
% @param StringResidues list of the residue constraints in a string format
residues_clean([], _ListResidueVariables, []).
residues_clean([Residu|RemainResidues], ListResidueVariables, [StringResidue|RemainStringResidues]) :-
    term_string(Residu, ClpfdString),
    replace_substring(ClpfdString, "clpfd: ", "", ClpfdString2),  % delete the prefix "clpfd: "
    replace_substring(ClpfdString2, "clpfd:", "", String),
    residues_clean_replace(String, ListResidueVariables, CleanedStringResidue),
    (CleanedStringResidue = String ->
         StringResidue = CleanedStringResidue;
         atomics_to_string([CleanedStringResidue, "\t\t\tNot cleaned constraint: ", String],
                           StringResidue)),
    residues_clean(RemainResidues, ListResidueVariables, RemainStringResidues).


%% residues_clean_replace(String, ListResidueVariables, CleanedString)
% Succeeds iff CleanedString is String
% with identified Prolog variables replaced their corresponding variable name.
%
% @param String a string corresponding to a residue constraint
% @param ListResidueVariables list of pairs variable name: residue Prolog variable
% @param CleanedString a string
residues_clean_replace(String, [], String).
residues_clean_replace(String,
                       [ResidueVariable:ResiduePrologVariable | RemainListResidueVariables],
                       CleanedString) :-
    term_string(ResidueVariable, StringResidueVariable),
    term_string(ResiduePrologVariable, StringResiduePrologVariable),
    replace_substring(String, StringResiduePrologVariable, StringResidueVariable, MiddleString),
    residues_clean_replace(MiddleString, RemainListResidueVariables, CleanedString).


%% split_variables(ListVariables, Uniques, NotUniques, Infinites)
% Succeeds iff ListVariables is the union (in the same order) of Uniques and NotUniques
% and Uniques contains only variable with a unique value,
% NotUniques contains only variable with more than one values but finite,
% Infinites contains only variable with infinite values.
%
% @param ListVariables list of pairs variable name: Prolog variable
% @param Uniques list of pairs variable name: Prolog variable
% @param NotUniques list of pairs variable name: Prolog variable
% @param Infinites list of pairs variable name: Prolog variable
split_variables([], [], [], []).
split_variables([Variable:PrologVariable | RemainListVariables],
                [Variable:PrologVariable | Uniques], NotUniques, Infinites) :-
    fd_size(PrologVariable, 1),
    split_variables(RemainListVariables, Uniques, NotUniques, Infinites).
split_variables([Variable:PrologVariable | RemainListVariables],
                Uniques, [Variable:PrologVariable | NotUniques], Infinites) :-
    fd_size(PrologVariable, Size),
    Size \= sup,
    Size > 1,
    split_variables(RemainListVariables, Uniques, NotUniques, Infinites).
split_variables([Variable:PrologVariable | RemainListVariables],
                Uniques, NotUniques, [Variable:PrologVariable | Infinites]) :-
    fd_size(PrologVariable, sup),
    split_variables(RemainListVariables, Uniques, NotUniques, Infinites).


%% all_finite(ListVariables)
% Succeeds iff all variables have a finite number of values.
%
% @param ListVariables list of pairs variable name: Prolog variable
all_finite(ListVariables) :-
    split_variables(ListVariables, _Uniques, _NotUniques, []).



% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Solves the example problem from the section 7 of the statement and write results. %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% run_example
% Solve the example problem and write results.
run_example :-
    lpsolve_analyze(["The variable x lies between 0 and 10",
                     "Variable y varies from 10 to -10",
                     "A variable z is in the range 0 to 15",
                     "It equals x plus y",
                     "All these variables are greater than -20",
                     "y is less than 5 + 2 * x",
                     "x is greater than y times 2",
                     "Variable y is greater than or equal to the quotient of z by 4"]).

% Output of run_example:
%
% ---------- More than one solution, but finite number of solutions ----------
% ----- Variable(s) with unique value -----
% ----- Variable(s) with more than one values but finite -----
% x in 1..10
% y in 0..4
% z in 1..14
% ----- Constraint residues -----
% [z:_G353,y:_G244,x:_G144]
% clpfd:in(_G5067,..(-2,14))
% clpfd: #=(_G5067+_G5070,_G5085)
% clpfd: #=(_G5067//4,_G5073)
% clpfd:in(_G5070,..(0,3))
% clpfd: #=(_G5085 mod 4,_G5070)
% clpfd:in(_G5085,..(1,14))
% clpfd: #=(_G5091+_G5088,_G5085)
% clpfd:in(_G5091,..(1,10))
% clpfd: #=<(_G5076,_G5091+ -1)
% clpfd: #=(2*_G5091,_G5082)
% clpfd:in(_G5076,..(0,8))
% clpfd: #=(_G5088*2,_G5076)
% clpfd:in(_G5088,..(0,4))
% clpfd: #>=(_G5088,_G5073)
% clpfd:in(_G5073,..(0,3))
% clpfd:in(_G5079,..(7,25))
% clpfd: #=(5+_G5082,_G5079)
% clpfd:in(_G5082,..(2,20))
% ----------



% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Workaround to have the call_residue/2 predicate. %
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% call_residue/2 exists in SICStus but not in SWI-Prolog.
% Here an implementation found inside this documentation page:
% http://www.swi-prolog.org/pldoc/doc/_SWI_/library/dialect/sicstus.pl?show=raw

:- meta_predicate
	call_residue(0, -).

%% call_residue(Goal, Residues)
% Note that this implementation is *incomplete*.
% Please consult the documentation of call_residue_vars/2 for known issues.
%
% @param Goal goal to analyze
% @param Residues list of VarSet-Goal
call_residue(Goal, Residues) :-
    call_residue_vars(Goal, Vars),
    (    Vars == []
     ->  Residues = []
     ;   copy_term(Vars, _AllVars, Goals),
	 phrase(vars_by_goal(Goals), Residues)
    ).

vars_by_goal((A,B)) --> !,
	                vars_by_goal(A),
	                vars_by_goal(B).
vars_by_goal(Goal) -->
    { term_attvars(Goal, AttVars),
      sort(AttVars, VarSet)
    },
    [ VarSet-Goal ].



% %%%%%%%%%%%%%%%%%%%%%%%%%%
% Helper predicate to test %
% %%%%%%%%%%%%%%%%%%%%%%%%%%

%% check_range(Range, Inf, Sup)
% Succeeds iff Range is the domain Inf..Sup.
%
% @param Range a finite integer domain that is a range
% @param Inf inf, sup or an integer
% @param Sup inf, sup or an integer
check_range(Range, inf, Sup) :-
    fd_inf(Range, inf),
    fd_sup(Range, Sup).
check_range(Range, Inf, sup) :-
    Inf \= inf,
    fd_inf(Range, Inf),
    fd_sup(Range, sup).
check_range(Range, Inf, Sup) :-
    Inf \= inf,
    Inf \= sup,
    Sup \= inf,
    Sup \= sup,
    fd_inf(Range, Inf),
    fd_sup(Range, Sup),
    Size is Sup + 1 - Inf,
    fd_size(Range, Size).



% %%%%%%%%%%%%
% Unit Tests %
% %%%%%%%%%%%%

:- begin_tests(test_helpers).

% Check checking of ranges.
test(check_range) :-
    R1 in -42..666,
    assertion(check_range(R1, -42, 666)),

    R2 in 42..666,
    assertion(\+ check_range(R2, -42, 666)),

    R3 in -42..sup,
    assertion(check_range(R3, -42, sup)),

    R4 in inf..666,
    assertion(check_range(R4, inf, 666)),

    R5 in inf..sup,
    assertion(check_range(R5, inf, sup)).


% Check comparison of ranges.
test(range_equals) :-
    A1 in -42..666,
    B1 in -42..666,
    assertion(range_equals(A1, B1)),

    A2 in 42..666,
    B2 in -42..666,
    assertion(\+ range_equals(A2, B2)).


% Check replace substring in a string.
test(replace_substring) :-
    assertion(replace_substring("abcdefghabcdefgh", "ab", "XYZ", "XYZcdefghXYZcdefgh")),
    assertion(replace_substring("abcdefghabcdefgh", "cd", "XYZ", "abXYZefghabXYZefgh")),
    assertion(replace_substring("abcdefghabcdefgh", "cd", "", "abefghabefgh")),
    assertion(replace_substring("abcdefghabcdefgh", "h", "XY", "abcdefgXYabcdefgXY")),
    assertion(replace_substring("abcdefghabcdefgh", "cd", "X", "abXefghabXefgh")),

    assertion(replace_substring("blabla", "la", "X", "bXbX")),
    assertion(replace_substring("bLabla", "la", "X", "bLabX")),
    assertion(replace_substring("blAbla", "la", "X", "blAbX")).

:- end_tests(test_helpers).



:- begin_tests(test_arithm).

% Checks simple arithmetical expressions (with mathematical notation or English language).
test(expression_simple) :-
    expression([], [], R1, [42, +, 50], []),
    assertion(R1 == 92),
    expression([], [], R1_2, [the, sum, of, 42, and, 50], []),
    assertion(R1_2 == 92),
    expression([], [], R2, [42, -, 50], []),
    assertion(R2 == -8),
    expression([], [], R2_2, [the, difference, of, 42, and, 50], []),
    assertion(R2_2 == -8),
    expression([], [], R3, [7, *, 3], []),
    assertion(R3 == 21),
    expression([], [], R3_2, [the, multiplication, of, 7, and, 3], []),
    assertion(R3_2 == 21),
    expression([], [], R4, [43, /, 3], []),
    assertion(R4 == 14),
    expression([], [], R4_2, [the, division, of, 43, by, 3], []),
    assertion(R4_2 == 14),
    expression([], [], R5, [43, \45, 3], []),
    assertion(R5 == 1),
    expression([], [], R5_2, [the, remainder, of, the, division, of, 43, by, 3], []),
    assertion(R5_2 == 1).


% Checks arithmetical expressions with negative numbers.
test(expression_simple_neg) :-
    expression([], [], R1, [42, +, -50], []),
    assertion(R1 == -8),
    expression([], [], R2, [42, -, -50], []),
    assertion(R2 == 92),
    expression([], [], R3, [7, *, -3], []),
    assertion(R3 == -21),
    expression([], [], R4, [43, /, -3], []),
    assertion(R4 == -15),
    expression([], [], R5, [43, \45, -3], []),
    assertion(R5 == 1).


% Checks more complicated arithmetical expressions and the respect of the operators priorities.
test(expression_compound) :-
    expression([], [], R1, [5, +, 2, *, 3], []),
    assertion(R1 == 11),
    expression([], [], R2, [5, *, 2, +, 3], []),
    assertion(R2 == 13),
    expression([], [], R3, [100, +, 43, /, 2], []),
    assertion(R3 == 121),
    expression([], [], R4, [43, /, 3, -, 100], []),
    assertion(R4 == -86),
    expression([], [], R5, [-3, +, 2, *, 7, -, -10, +, 3, *, 0], []),
    assertion(R5 == 21).


% Checks if division by 0 throws the appropriate exception.
test(expression_div_0, [error(evaluation_error(zero_divisor))]) :-
    expression([], [], _R, [42, /, 0], []).


% Checks if the remainder of the division by 0 throws the appropriate exception.
test(expression_rem_0, [error(evaluation_error(zero_divisor))]) :-
    expression([], [], _R, [42, \45, 0], []).

:- end_tests(test_arithm).



:- begin_tests(test_variable).

% Checks is_variable.
test(is_variable) :-
    is_variable(V1, [x], []),
    assertion(ground(V1)),
    assertion(V1 == x),
    assertion(\+ is_variable(_V2, [X], [])),  % produces a warning singleton variable
    assertion(\+ is_variable(_V3, [_], [])),
    assertion(\+ is_variable(_V4, [_x], [])),  % produces a warning singleton variable
    Y = y,
    is_variable(V5, [Y], []),
    assertion(V5 == y).

:- end_tests(test_variable).



:- begin_tests(test_sentence).

% Checks sentence_equal.
test(sentence) :-
    sentence("x = 3 + 10 * -2", [], VariablesOut1, false, LastVariableOut1),
    listvariables_get(VariablesOut1, Variable1, PrologVariable1),
    assertion(VariablesOut1 == [x: -17]),
    assertion(LastVariableOut1 == x),
    assertion(Variable1 == x),
    assertion(PrologVariable1 == -17),

    sentence("x is greater than 3 + 10 * -2", [], VariablesOut2, false, LastVariableOut2),
    listvariables_get(VariablesOut2, Variable2, PrologVariable2),
    assertion(VariablesOut2 == [x: PrologVariable2]),
    assertion(LastVariableOut2 == x),
    assertion(Variable2 == x),
    check_range(PrologVariable2, -16, sup),

    sentence("x is less than 3 + 10 * -2", [], VariablesOut3, false, LastVariableOut3),
    listvariables_get(VariablesOut3, Variable3, PrologVariable3),
    assertion(VariablesOut3 == [x: PrologVariable3]),
    assertion(LastVariableOut3 == x),
    assertion(Variable3 == x),
    check_range(PrologVariable3, inf, -18),

    sentence("x is greater than or equal to 3 + 10 * -2", [], VariablesOut4, false, LastVariableOut4),
    listvariables_get(VariablesOut4, Variable4, PrologVariable4),
    assertion(VariablesOut4 == [x: PrologVariable4]),
    assertion(LastVariableOut4 == x),
    assertion(Variable4 == x),
    check_range(PrologVariable4, -17, sup),

    sentence("x is less than or equal to 3 + 10 * -2", [], VariablesOut5, false, LastVariableOut5),
    listvariables_get(VariablesOut5, Variable5, PrologVariable5),
    assertion(VariablesOut5 == [x: PrologVariable5]),
    assertion(LastVariableOut5 == x),
    assertion(Variable5 == x),
    check_range(PrologVariable5, inf, -17),

    sentence("x lies between 1 + 2 and 2 * 3 + 1", [], VariablesOut6, false, LastVariableOut6),
    listvariables_get(VariablesOut6, Variable6, PrologVariable6),
    assertion(VariablesOut6 == [x: PrologVariable6]),
    assertion(LastVariableOut6 == x),
    assertion(Variable6 == x),
    check_range(PrologVariable6, 3, 7),

    sentence("x is in the range 2 * 3 + 1 to 1 + 2", [], VariablesOut7, false, LastVariableOut7),
    listvariables_get(VariablesOut7, Variable7, PrologVariable7),
    assertion(VariablesOut7 == [x: PrologVariable7]),
    assertion(LastVariableOut7 == x),
    assertion(Variable7 == x),
    check_range(PrologVariable7, 3, 7),

    sentence("x is in 2 * 3 + 1 .. 1 + 2", [], VariablesOut8, false, LastVariableOut8),
    listvariables_get(VariablesOut8, Variable8, PrologVariable8),
    assertion(VariablesOut8 == [x: PrologVariable8]),
    assertion(LastVariableOut8 == x),
    assertion(Variable8 == x),
    check_range(PrologVariable8, 3, 7).


% Checks if some wrong sentences fail.
test(sentence) :-
    \+ sentence("x 3 + 10 * -2", [], _, false, _),  % missing "="
    \+ sentence("xx = 3 + 10 * -2", [], _, false, _),  % invalid variable name
    \+ sentence("x = + 10 * -2", [], _, false, _),  % missing left operand

    \+ lpsolve(["Variable x varies ffrom 1 to 20"], _),  % misspelled "from"
    \+ lpsolve(["Variable x from 1 to 20"], _),  % missing "varies"
    \+ lpsolve(["Variable x varies from 1 to"], _),  % misspelled right expression

    \+ lpsolve(["y between -2 and 25"], _),  % missing is
    \+ lpsolve(["y is between -2 aand 25"], _),  % misspelled aand
    \+ lpsolve(["y is oups between -2 and 25"], _),  % additional wrong word "oups"
    \+ lpsolve(["x y is between -2 and 25"], _),  % two variable names
    \+ lpsolve(["y is oups between -2 and 25 666"], _),  % additional wrong number
    \+ lpsolve(["42 is between -2 and 25"], _),  % number in a wrong place

    \+ lpsolve(["Vari able w holds the result of dividing z by 2"], _),  % misspelled "Variable"
    \+ lpsolve(["Variable w holds result of dividing z by 2"], _),  % missing "the"
    \+ lpsolve(["Variable w holds the result of quotient z by 2"], _),  % wrong word "quotient"

    \+ lpsolve([""], _).  % empty sentence

:- end_tests(test_sentence).



:- begin_tests(test_sentences).

% Check few sentence_equal.
test(sentence_equal) :-
    lpsolve(["x = 3 + 10 * -2"], VariablesOut1),
    assertion(VariablesOut1 == [x: -17]),

    lpsolve(["x = 3 + 10 * -2",
             "y = 100",
             "z = y / x"], VariablesOut2),
    assertion(VariablesOut2 == [z: -6, y: 100, x: -17]),

    lpsolve(["x = 3 + 10 * -2",
             "y = 100",
             "z = y % x"], VariablesOut3),
    assertion(VariablesOut3 == [z: 15, y: 100, x: -17]),

    lpsolve(["x = -15 - 10 * -2",
             "y lies between 10 and 100",
             "z = y % x"], VariablesOut4),
    listvariables_get(VariablesOut4, x, X4),
    listvariables_get(VariablesOut4, y, Y4),
    listvariables_get(VariablesOut4, z, Z4),
    assertion(X4 == 5),
    assertion(check_range(Y4, 10, 100)),
    assertion(check_range(Z4, 0, 4)).


% Checks separately each sentence of the syntax examples
% from the section 3 of the project statement.
test(sentences__isolated_examples_section_3) :-
    lpsolve(["The variable x lies between 0 and 10"], [x: X1]),
    assertion(check_range(X1, 0, 10)),

    lpsolve(["Variable x varies from 1 to 20"], [x: X2]),
    assertion(check_range(X2, 1, 20)),

    lpsolve(["A variable x is in the range 100 to 14"], [x: X3]),
    assertion(check_range(X3, 14, 100)),

    lpsolve(["y is between -2 and 25"], [y: Y4]),
    assertion(check_range(Y4, -2, 25)),

    lpsolve(["x is in -25 .. -50"], [x: X5]),
    assertion(check_range(X5, -50, -25)),


    lpsolve(["x equals a plus b"], [b: B11,
                                      a: A11,
                                      x: X11]),
    assertion(check_range(X11, inf, sup)),
    assertion(check_range(A11, inf, sup)),
    assertion(check_range(B11, inf, sup)),

    lpsolve(["a is in 10 .. 50",
             "b is in 100 .. 200",
             "x equals a plus b"], [x: X12,
                                    b: B12,
                                    a: A12]),
    assertion(check_range(A12, 10, 50)),
    assertion(check_range(B12, 100, 200)),
    assertion(check_range(X12, 110, 250)),

    lpsolve(["x is c times 2"], [c: C13,
                                   x: X13]),
    assertion(check_range(X13, inf, sup)),
    assertion(check_range(C13, inf, sup)),

    lpsolve(["c is in 10 .. 50",
               "x is c times 2"], [x: X14,
                                   c: C14]),
    assertion(check_range(C14, 10, 50)),
    assertion(check_range(X14, 20, 100)),

    lpsolve(["The variable z contains the product of b and c"], [c: C15,
                                                                 b: B15,
                                                                 z: Z15]),
    assertion(check_range(B15, inf, sup)),
    assertion(check_range(C15, inf, sup)),
    assertion(check_range(Z15, inf, sup)),

    lpsolve(["b is in 2 .. 4",
             "c is in 10 .. 50",
             "The variable z contains the product of b and c"], [z: Z16,
                                                                 c: C16,
                                                                 b: B16]),
    assertion(check_range(B16, 2, 4)),
    assertion(check_range(C16, 10, 50)),
    assertion(check_range(Z16, 20, 200)),

    lpsolve(["y is less than 5 + 2 * q"], [q: Q21,
                                           y: Y21]),
    assertion(check_range(Y21, inf, sup)),
    assertion(check_range(Q21, inf, sup)),

    lpsolve(["q is in 1 .. 3",
             "y is less than 5 + 2 * q"], [y: Y22,
                                           q: Q22]),
    assertion(check_range(Q22, 1, 3)),
    assertion(check_range(Y22, inf, 10)),

    lpsolve(["All these variables are greater than 5"], []),

    lpsolve(["x > 0",
             "All these variables are greater than 5"], [x: X31]),
    assertion(check_range(X31, 6, sup)),

    lpsolve(["x > 0",
             "y > 100",
             "z < 100",
             "All these variables are greater than 5"], [z: Z32,
                                                         y: Y32,
                                                         x: X32]),
    assertion(check_range(X32, 6, sup)),
    assertion(check_range(Y32, 101, sup)),
    assertion(check_range(Z32, 6, 99)),

    lpsolve(["Variable q is greater than or equal to the quotient of z and 2"], [z: Z41,
                                                                                 q: Q41]),
    assertion(check_range(Q41, inf, sup)),
    assertion(check_range(Z41, inf, sup)),

    lpsolve(["z is in 15 .. 29",
             "Variable q is greater than or equal to the quotient of z and 2"], [q: Q42,
                                                                                 z: Z42]),
    assertion(check_range(Z42, 15, 29)),
    assertion(check_range(Q42, 7, sup)),

    lpsolve(["Variable w holds the result of dividing z by 2"], [z: Z43,
                                                                 w: W43]),
    assertion(check_range(W43, inf, sup)),
    assertion(check_range(Z43, inf, sup)),

    lpsolve(["z is in 15 .. 29",
             "Variable w holds the result of dividing z by 2"], [w: W43,
                                                                 z: Z43]),
    assertion(check_range(Z43, 15, 29)),
    assertion(check_range(W43, 7, 14)),

    lpsolve(["z is in 15 .. 29",
             "Variable q is greater than or equal to the quotient of z and 2",
             "Variable w holds the result of dividing z by 2"], [w: W44,
                                                                 q: Q44,
                                                                 z: Z44]),
    assertion(check_range(Z44, 15, 29)),
    assertion(check_range(Q44, 7, sup)),
    assertion(check_range(W44, 7, 14)),

    lpsolve(["z is in 15 .. 29",
             "Variable q is greater than or equal to the quotient of z and 2",
             "Variable w holds the result of dividing z by 2",
             "It is greater than q"], [w: W51,
                                       q: Q51,
                                       z: Z51]),
    assertion(check_range(Z51, 16, 28)),
    assertion(check_range(Q51, 7, 13)),
    assertion(check_range(W51, 8, 14)).


% Checks if "Variable ..." and "The variable ..." succeed even if the variable  already exist.
test(sentences__variable) :-
    lpsolve(["x = 42",
             "Variable x is in the range 100 to 14"], [x: X1]),
    assertion(X1 == 42),

    lpsolve(["x = 42",
             "The variable x is in the range 100 to 14"], [x: X2]),
    assertion(X2 == 42),

    lpsolve(["x = 42",
             "x is in the range 100 to 14"], [x: X3]),
    assertion(X3 == 42).


% Checks if "A variable ..." already existing throws the appropriate exception.
test(sentences__a_variable_exception, [error(evaluation_error(a_variable_already_exist))]) :-
    lpsolve(["x = 42",
             "A variable x is in the range 100 to 14"], _).


% Checks if "A variable ..." already existing throws the appropriate exception
% even if the constraints failed.
test(sentences__a_variable_exception_2, [error(evaluation_error(a_variable_already_exist))]) :-
    lpsolve(["x = 0",
             "A variable x is in the range 100 to 14"], _).


% Checks if "It ..." without previous variable throws the appropriate exception.
test(sentences__it_exception, [error(evaluation_error(it_with_unknown_variable))]) :-
    lpsolve(["It is greater than q"], _).

test(sentences__it_exception, [error(evaluation_error(it_with_unknown_variable))]) :-
    lpsolve(["x >= 3",
             "All these variables are 666",
             "It is greater than q"], _).


% Checks a modified set of the sentences from the section 7 of the project statement
% that has no solution.
test(sentences__example_section_7_modified_without_solution) :-
    \+ lpsolve(["The variable x lies between 0 and 10",
                "Variable y varies from 10 to -10",
                "A variable z is in the range 0 to 15",
                "It z equals x plus y",
                "All these variables are greater than -20",
                "y is less than 5 + 2 * x",
                "x is greater than y times 2",
                "Variable y is greater than or equal to the quotient of z by 4",
                "y < 0"],
               _VariablesOut).

:- end_tests(test_sentences).



:- begin_tests(test_example_section_7).

%% Checks the set of sentences example from the section 7 of the project statement.
% Solutions are:
%   x in 1..10
%   y in 0..4
%   z in 1..14
test(sentences__example_section_7) :-
    call_residue(
            lpsolve(["The variable x lies between 0 and 10",
                     "Variable y varies from 10 to -10",
                     "A variable z is in the range 0 to 15",
                     "It equals x plus y",
                     "All these variables are greater than -20",
                     "y is less than 5 + 2 * x",
                     "x is greater than y times 2",
                     "Variable y is greater than or equal to the quotient of z by 4"],
                    VariablesOut),
            Residues),
    write("\n"),
    analyze(VariablesOut, Residues),
    [z: Z,
     y: Y,
     x: X] = VariablesOut,
    assertion(check_range(X, 1, 10)),
    assertion(check_range(Y, 0, 4)),
    assertion(check_range(Z, 1, 14)),

    lpsolve([[the, variable, x, lies, between, 0, and, 10],
             [variable, y, varies, from, 10, to, -10],
             [a, variable, z, is, in, the, range, 0, to, 15],
             [it, equals, x, plus, y],
             [all, these, variables, are, greater, than, -20],
             [y, is, less, than, 5, +, 2, *, x],
             [x, is, greater, than, y, times, 2],
             [variable, y, is, greater, than, or, equal, to, the, quotient, of, z, by, 4]],
            VariablesOut2),
    [z: Z2,
     y: Y2,
     x: X2] = VariablesOut2,
    assertion(check_range(X2, 1, 10)),
    assertion(check_range(Y2, 0, 4)),
    assertion(check_range(Z2, 1, 14)).

:- end_tests(test_example_section_7).



:- begin_tests(test_example_section_7_modified_with_unique_variable).

% Checks a modified set of the sentences from the section 7 of the project statement
% that contains a variable with an unique solution.
% Solutions are:
%   x in 9..10
%   y = 4
%   z in 13..14
test(sentences__example_section_7_modified_unique_variable) :-
    call_residue(
            lpsolve(["The variable x lies between 0 and 10",
                     "Variable y varies from 10 to -10",
                     "A variable z is in the range 0 to 15",
                     "It equals x plus y",
                     "All these variables are greater than -20",
                     "y is less than 5 + 2 * x",
                     "x is greater than y times 2",
                     "Variable y is greater than or equal to the quotient of z by 4",
                     "y > 3"],
                    VariablesOut),
            Residues),
    write("\n"),
    analyze(VariablesOut, Residues),
    [z: Z,
     y: Y,
     x: X] = VariablesOut,
    assertion(check_range(X, 9, 10)),
    assertion(Y == 4),
    assertion(check_range(Z, 13, 14)).

:- end_tests(test_example_section_7_modified_with_unique_variable).



:- begin_tests(test_example_section_7_modified_with_unique_variable_and_infinite_variable).

% Checks a modified set of the sentences from the section 7 of the project statement
% that contains a variable with an unique solution.
% Solutions are:
%   a in 0..sup
%   x in 9..10
%   y = 4
%   z in 13..14
test(sentences__example_section_7_modified_unique_variable_and_infinite_variable) :-
    call_residue(
            lpsolve(["The variable x lies between 0 and 10",
                     "Variable y varies from 10 to -10",
                     "A variable z is in the range 0 to 15",
                     "It equals x plus y",
                     "All these variables are greater than -20",
                     "y is less than 5 + 2 * x",
                     "x is greater than y times 2",
                     "Variable y is greater than or equal to the quotient of z by 4",
                     "y > 3",
                     "a >= 0"],
                    VariablesOut),
            Residues),
    write("\n"),
    analyze(VariablesOut, Residues),
    [a: A,
     z: Z,
     y: Y,
     x: X] = VariablesOut,
    assertion(check_range(A, 0, sup)),
    assertion(check_range(X, 9, 10)),
    assertion(Y == 4),
    assertion(check_range(Z, 13, 14)).

:- end_tests(test_example_section_7_modified_with_unique_variable_and_infinite_variable).



:- begin_tests(test_example_section_7_modified_unique_solution).

% Checks a modified set of the sentences from the section 7 of the project statement
% that has one unique solution.
% Solution is:
%   x = 10
%   y = 4
%   z = 14
test(sentences__example_section_7_modified_unique_solution) :-
    call_residue(
            lpsolve(["The variable x lies between 0 and 10",
                     "Variable y varies from 10 to -10",
                     "A variable z is in the range 0 to 15",
                     "It equals x plus y",
                     "All these variables are greater than -20",
                     "y is less than 5 + 2 * x",
                     "x is greater than y times 2",
                     "Variable y is greater than or equal to the quotient of z by 4",
                     "z > 13"],
                    VariablesOut),
            Residues),
    write("\n"),
    analyze(VariablesOut, Residues),
    [z: Z,
     y: Y,
     x: X] = VariablesOut,
    assertion(X == 10),
    assertion(Y == 4),
    assertion(Z == 14).

:- end_tests(test_example_section_7_modified_unique_solution).



% %%%%%%%%%%%%%%%%%%%%%%%%%%
% Output of the unit tests %
% %%%%%%%%%%%%%%%%%%%%%%%%%%

% Welcome to SWI-Prolog (Multi-threaded, 64 bits, Version 7.2.3)
% Copyright (c) 1990-2015 University of Amsterdam, VU Amsterdam
% SWI-Prolog comes with ABSOLUTELY NO WARRANTY. This is free software,
% and you are welcome to redistribute it under certain conditions.
% Please visit http://www.swi-prolog.org for details.
%
% For help, use ?- help(Topic). or ?- apropos(Word).
%
% ?- [linprog].
% Warning: /home/opi/data/hg/linprog/linprog.pl:1085:   [produced by the unit test test(variable)]
%         Singleton variables: [X,_x]
% true.
%
% ?- run_tests.
% % PL-Unit: test_helpers ... done
% % PL-Unit: test_arithm
% Warning: /home/opi/data/hg/linprog/linprog.pl:1018:
%         PL-Unit: Test expression_simple: Test succeeded with choicepoint
% Warning: /home/opi/data/hg/linprog/linprog.pl:1042:
%         PL-Unit: Test expression_simple_neg: Test succeeded with choicepoint
% Warning: /home/opi/data/hg/linprog/linprog.pl:1056:
%         PL-Unit: Test expression_compound: Test succeeded with choicepoint
% .. done
% % PL-Unit: test_variable . done
% % PL-Unit: test_sentence
% Warning: /home/opi/data/hg/linprog/linprog.pl:1103:
%         PL-Unit: Test sentence: Test succeeded with choicepoint
% . done
% % PL-Unit: test_sentences
% Warning: /home/opi/data/hg/linprog/linprog.pl:1191:
%         PL-Unit: Test sentence_equal: Test succeeded with choicepoint
% Warning: /home/opi/data/hg/linprog/linprog.pl:1218:
%         PL-Unit: Test sentences__isolated_examples_section_3: Test succeeded with choicepoint
% Warning: /home/opi/data/hg/linprog/linprog.pl:1348:
%         PL-Unit: Test sentences__variable: Test succeeded with choicepoint
% ..... done
% % PL-Unit: test_example_section_7
% ---------- More than one solution, but finite number of solutions ----------
% ----- Variable(s) with unique value -----
% ----- Variable(s) with more than one values but finite -----
% x in 1..10
% y in 0..4
% z in 1..14
% ----- Constraint residues -----
% in(_G4286,..(-2,14))
% #=(_G4286+_G4289,z)                        Not cleaned constraint: #=(_G4286+_G4289,_G4304)
% #=(_G1774//4,_G1776)
% in(_G1775,..(0,3))
% #=(_G1780 mod 4,_G1775)
% in(z,..(1,14))                        Not cleaned constraint: in(_G1277,..(1,14))
% #=(x+y,z)                        Not cleaned constraint: #=(_G1279+_G1278,_G1277)
% in(x,..(1,10))                        Not cleaned constraint: in(_G1279,..(1,10))
% #=<(_G1274,x+ -1)                        Not cleaned constraint: #=<(_G1274,_G1279+ -1)
% #=(2*x,_G1276)                        Not cleaned constraint: #=(2*_G1279,_G1276)
% in(_G1274,..(0,8))
% #=(y*2,_G1274)                        Not cleaned constraint: #=(_G1278*2,_G1274)
% in(y,..(0,4))                        Not cleaned constraint: in(_G1278,..(0,4))
% #>=(y,_G1273)                        Not cleaned constraint: #>=(_G1278,_G1273)
% in(_G1273,..(0,3))
% in(_G1275,..(7,25))
% #=(5+_G1276,_G1275)
% in(_G1276,..(2,20))
% ----------
% Warning: /home/opi/data/hg/linprog/linprog.pl:1410:
%         PL-Unit: Test sentences__example_section_7: Test succeeded with choicepoint
%  done
% % PL-Unit: test_example_section_7_modified_with_unique_variable
% ---------- More than one solution, but finite number of solutions ----------
% ----- Variable(s) with unique value -----
% y = 4
% ----- Variable(s) with more than one values but finite -----
% x in 9..10
% z in 13..14
% ----- Constraint residues -----
% in(_G10631,..(10,14))
% #=(_G10631+_G10634,z)                        Not cleaned constraint: #=(_G10631+_G10634,_G10646)
% #=(_G10631//4,_G10637)
% in(_G10634,..(0,3))
% #=(z mod 4,_G10634)                        Not cleaned constraint: #=(_G10646 mod 4,_G10634)
% in(z,..(13,14))                        Not cleaned constraint: in(_G10646,..(13,14))
% #=(x+4,z)                        Not cleaned constraint: #=(_G10649+4,_G10646)
% in(x,..(9,10))                        Not cleaned constraint: in(_G10649,..(9,10))
% #=(2*x,_G10643)                        Not cleaned constraint: #=(2*_G10649,_G10643)
% in(_G10643,..(18,20))
% #=(5+_G10643,_G10640)
% in(_G10640,..(23,25))
% in(_G10637,..(2,3))
% ----------
% Warning: /home/opi/data/hg/linprog/linprog.pl:1459:
%         PL-Unit: Test sentences__example_section_7_modified_unique_variable: Test succeeded with choicepoint
%  done
% % PL-Unit: test_example_section_7_modified_with_unique_variable_and_infinite_variable
% ---------- Maybe an infinite number of solutions ----------
% ----- Variable(s) with unique value -----
% y = 4
% ----- Variable(s) with more than one values but finite -----
% x in 9..10
% z in 13..14
% ----- Variable(s) with infinite numbers of values -----
% a in 0..sup
% ----------
% Warning: /home/opi/data/hg/linprog/linprog.pl:1494:
%         PL-Unit: Test sentences__example_section_7_modified_unique_variable_and_infinite_variable: Test succeeded with choicepoint
%  done
% % PL-Unit: test_example_section_7_modified_unique_solution
% ---------- Unique solution ----------
% x = 10
% y = 4
% z = 14
% ----------
% Warning: /home/opi/data/hg/linprog/linprog.pl:1531:
%         PL-Unit: Test sentences__example_section_7_modified_unique_solution: Test succeeded with choicepoint
%  done
% % All 23 tests passed
% true.
%
% ?-
% % halt
